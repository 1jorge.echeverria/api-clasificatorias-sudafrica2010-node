import {
  getAllMatchesService,
  getChilePointsService,
  getMatchByIdService,
} from "../services/match-service.mjs";

export const getAllMatches = async (req, res) => {
  try {
    return res.status(200).send(await getAllMatchesService());
  } catch (error) {
    console.error(error);
  }
};

export const getMatchById = async (req, res) => {
  const {
    params: { matchId },
  } = req;

  if (!matchId) {
    res.status(400).send({
      status: "FAILED",
      data: { error: "Parameter 'matchId' can not be empty" },
    });
    return;
  }

  try {
    return res.send({ status: 200, data: await getMatchByIdService(matchId) });
  } catch (error) {
    return res.send({ status: 500, data: error });
  }
};

export const getChilePoints = async (req, res) => {
  try {
    return res.status(200).send({ data: await getChilePointsService() });
  } catch (error) {
    console.error(error);
  }
};
