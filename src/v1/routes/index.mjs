import express from "express";
import {
  getAllMatches,
  getChilePoints,
  getMatchById,
} from "../../controllers/matches-controller.mjs";

export const router = express.Router();

router.get("/", getAllMatches);
router.get("/total-points", getChilePoints);
router.get("/:matchId", getMatchById);
