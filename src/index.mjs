import express from "express";
import morgan from "morgan";
import chalk from "chalk";

import { router } from "./v1/routes/index.mjs";
import { clearDatabase } from "./database/open-sqlitedb.mjs";
import { initializeDatabase } from "./database/open-sqlitedb.mjs";

const app = express();

//Configuraciones
app.set("port", process.env.PORT ?? 3000);
app.set("json spaces", 2);

//Middleware
app.use(morgan("dev"));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

//Router
app.use("/api/v1/matches", router);

//Iniciando el servidor
app.listen(app.get("port"), async () => {
  printEndpoints();
  await initializeDatabase();
});

//Vaciar BD al detener el API
process.on("SIGINT", async () => {
  await clearDatabase();
  process.exit();
});

function printEndpoints() {
  console.log(
    chalk.green("\n➜  Matches: ") +
      chalk.blue(`http://localhost:${app.get("port")}/api/v1/matches`) +
      chalk.green("\n➜  Total Points of Chile: ") +
      chalk.blue(
        `http://localhost:${app.get("port")}/api/v1/matches/total-points\n`
      )
  );
}
