import {
  getAllMatchesFromDB,
  getMatchByIdFromDB,
} from "../database/open-sqlitedb.mjs";

export async function getAllMatchesService() {
  try {
    return await getAllMatchesFromDB();
  } catch (error) {
    console.error("Error al abrir la base de datos:", error);
  }
}

export async function getMatchByIdService(matchId) {
  try {
    return await getMatchByIdFromDB(matchId);
  } catch (error) {
    console.error("Error al abrir la base de datos:", error);
  }
}

export async function getChilePointsService() {
  try {
    const matchesList = await getAllMatchesFromDB();
    let chilePoints = 0;
    matchesList.forEach((match) => {
      chilePoints += match["chilePoints"];
    });
    return chilePoints;
  } catch (error) {
    console.log(error);
  }
}
