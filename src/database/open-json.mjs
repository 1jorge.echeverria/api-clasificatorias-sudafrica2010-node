import fs from "node:fs/promises";

const MATCHES_JSON_PATH = "./src/database/partidos.json";

export async function getMatchesFromJSON() {
  try {
    const data = await fs.readFile(MATCHES_JSON_PATH, "utf8");
    return assignAdditionalProperties(JSON.parse(data));
  } catch (err) {
    console.error(err);
  }
}

function assignAdditionalProperties(matchesList) {
  let matchesListWithAllProperties = [];

  matchesList.forEach((match) => {
    const chileHomeTeamValue =
      match["matchStadium"] == "Estadio Nacional, Santiago";

    matchesListWithAllProperties.push({
      ...match,
      chileHomeTeam: chileHomeTeamValue,
      chilePoints: calculatePointsPerMatch(match, chileHomeTeamValue),
    });
  });
  return matchesListWithAllProperties;
}

function calculatePointsPerMatch(match, chileHomeTeam) {
  const [homeTeamGoals, visitingTeamGoals] = match["matchScore"].split("-");

  if (
    (homeTeamGoals > visitingTeamGoals && chileHomeTeam) ||
    (visitingTeamGoals > homeTeamGoals && !chileHomeTeam)
  ) {
    return 3;
  } else if (homeTeamGoals === visitingTeamGoals) {
    return 1;
  } else {
    return 0;
  }
}
