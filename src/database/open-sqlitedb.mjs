import { Sequelize, DataTypes } from "sequelize";
import { getMatchesFromJSON } from "./open-json.mjs";

const sequelize = new Sequelize({
  dialect: "sqlite",
  storage: "./src/database/partidos-datefix.db",
});

const Match = sequelize.define("Match", {
  matchDate: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  matchRival: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  matchScore: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  matchStadium: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  chileHomeTeam: {
    type: DataTypes.BOOLEAN,
    allowNull: false,
  },
  chilePoints: {
    type: DataTypes.INTEGER,
    allowNull: false,
  },
});

sequelize.sync();

export async function initializeDatabase() {
  const matchesList = await getMatchesFromJSON();
  await insertMatchesIntoDB(matchesList);
}

export async function getAllMatchesFromDB() {
  return await Match.findAll();
}

export async function getMatchByIdFromDB(matchId) {
  return await Match.findByPk(matchId);
}

export async function insertMatchesIntoDB(matchesList) {
  try {
    await Match.bulkCreate(matchesList);
  } catch (error) {
    console.log("\n ❌ Data insertion error!");
  }
}

export async function clearDatabase() {
  try {
    await Match.truncate();
    console.log("\n ⚠ Database cleared!");
  } catch (error) {
    console.log("\n ❌ Data deletion error: ", error);
  }
}
